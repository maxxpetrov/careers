﻿using OpenQA.Selenium;
using System.Collections.ObjectModel;
using System.Threading;

namespace Careers.Test
{
    class CareersPage
    {
        private IWebDriver driver;

        public CareersPage(IWebDriver drv)
        {
            driver = drv;
        }

        public IWebElement CountryDropdown => driver.FindElement(By.CssSelector("div.selecter.closed"));
        public IWebElement LanguageDropdown => driver.FindElement(By.Id("language"));
        public IWebElement LanguageSubmitButton => driver.FindElement(By.Id("language")).FindElement(By.CssSelector("a.selecter-fieldset-submit"));
        public IWebElement MoreVacanciesButton => driver.FindElement(By.CssSelector("div#index-vacancies-buttons a.load-more-button"));
        public ReadOnlyCollection<IWebElement> Vacancies => driver.FindElements(By.CssSelector("div.vacancies-blocks-item"));

        public void SelectCountry(string country)
        {
            CountryDropdown.Click();
            driver.FindElement(By.CssSelector($"span.selecter-item[data-value='{country}']")).Click();
        }
        public void SelectLanguage(Languages language)
        {
            LanguageDropdown.Click();
            driver.FindElement(By.CssSelector($"input#ch-{(int)language} + span")).Click();
        }

        public int CountVacancies()
        {
            MoreVacanciesButton.Click();
            Thread.Sleep(3000);
            return Vacancies.Count;
        }
    }
}
