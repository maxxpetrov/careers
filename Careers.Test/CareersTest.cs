﻿using OpenQA.Selenium.Chrome;
using System.IO;
using System.Reflection;
using Xunit;

namespace Careers.Test
{
    public class CareersTest
    {
        [Theory]
        [InlineData("Romania", Languages.English, 31)]
        public void ChooseCountryLanguage_ShouldFindRightAmountVacancies(string country, Languages language, int expected)
        {
            using (var driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)))
            {
                // Arrange
                driver.Manage().Window.Maximize();
                driver.Navigate().GoToUrl(@"https://careers.veeam.com/");
                CareersPage page = new CareersPage(driver);
                
                //Act
                page.SelectCountry(country);
                page.SelectLanguage(language);
                int actual = page.CountVacancies();
                
                //Assert
                Assert.Equal(expected, actual);
                
            }

        }
    }
}